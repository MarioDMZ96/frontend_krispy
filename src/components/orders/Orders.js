import React, { useEffect, useState } from "react";
import "./orders.css";
import axios from "axios";
import urlApi from "../../util/urlApi";
import { CardOrder } from "../common/cardOrder";
import { MenuBar } from "../common/menuBar";

export const Orders = () => {
  const [ordersList, setOrders] = useState([]);

  useEffect(() => {
    if (!localStorage.getItem("auth_token")) {
      window.location.href = "/";
    } else {
      getOrders();
    }
  }, []);

  const getOrders = async () => {
    try {
      const response = await axios.get(urlApi + "/orders", {
        headers: {
          Authorization: `${localStorage.getItem("auth_token")}`,
        },
      });
      if (response.data.orders.length === 0) {
        alert("No hay ordenes!");
      }
      setOrders(response.data.orders[0]);
    } catch (error) {
      alert("Ocurrio un error al consultar las ordenes!");
    }
  };

  return (
    <>
    <MenuBar />
    <h1>Lista de ordenes</h1>
      {
        ordersList.length === 0 ? 'Cargando...' :
        ordersList.map( item => {
          return <CardOrder
            key = {item.id}
            orderNumber = {item.order_number}
            total = { item.total }
            customer = { item.customer }
            customer_phone = { item.customer_phone }
            status = { item.status }
            items = { item.items[0] }
            payment = { item.payment_method }
            subTotal = { item.sub_total }
            discount = { item.discount }
            discountCode = { item.promo_code }
            deliveryMan = { item.delivery_man }
            deliveries = { item.deliveries }
            id = { item.id }
            changeStatus = { getOrders }
          />
        })
      }
    </>
  );
};

