import React, { useEffect, useState } from "react";
import "./login.css";
import axios from "axios";
import urlApi from '../../util/urlApi';

export const LoginForm = () => {
  
  const [formValues, setFormValues] = useState({ email: "", password: "" });
  
  const signIn = async () => {
    try {
      const response = await axios.post(urlApi + '/signin',formValues);
      if ( !response.data.token ){
        alert(response.data.message);
      } else {
        alert(response.data.message);
        localStorage.setItem('auth_token', response.data.token);
        window.location.href = '/orders';
      }
    } catch (error) {
      console.log(error);
      alert('Ha ocurrido un error al intentar inicar sesión!');
    }
  };

  useEffect( () => {
    if ( localStorage.getItem('auth_token') ){
      window.location.href = '/orders';
    }
  }, []);

  const handleChange = (e) =>
    setFormValues({
      ...formValues,
      [e.target.name]: e.target.value,
    });

  return (
    <div className="global-container">
      <div className="card login-form">
        <div className="card-body">
          <h3 className="card-title text-center">Inicia Sesión</h3>
          <div className="card-text">
            <div className="form-group">
              <label>Usuario: </label>
              <br />
              <input
                type="email"
                className="form-control"
                name="email"
                onChange={handleChange}
              />
              <br />
              <label>Contraseña: </label>
              <br />
              <input
                type="password"
                className="form-control"
                name="password"
                onChange={handleChange}
              />
              <br />
              <button className="btn btn-primary" onClick={signIn}>
                Iniciar Sesión
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
