import React from "react";
import "./styles.css";

export const MenuBar = () => {

  const handleLogOut = () => {
    localStorage.removeItem('auth_token');
    window.location.href = '/';
  }
  
  return (
    <div className="nav">
      <div className="nav-links">
        <a
          style = {{"cursor": 'pointer',"float": "left"}}
          onClick = { handleLogOut }
        >
          Cerrar sesión
        </a>
      </div>
    </div>
  );
};
