import React from "react";
import orderStatus from "../../util/orderStatus";
import axios from "axios";
import urlApi from "../../util/urlApi";

export const CardOrder = (data) => {
  const {
    orderNumber,
    total,
    customer,
    customer_phone,
    status,
    items,
    payment,
    subTotal,
    discount,
    discountCode,
    deliveryMan,
    deliveries,
    id,
    changeStatus,
  } = data;

  return (
    <>
      <div className="Card1">
        <div className="photo"></div>
        <ul className="details">
          <h4>Detalles de la orden</h4>
          <li>Metodo de pago: {payment.toUpperCase()}</li>
          <li>Subtotal: ${subTotal}</li>
          <li>Total: ${total}</li>
          {discount ? <li> Descuento: ${discount} </li> : null}
          {discountCode ? <li>Cupon: {discountCode} </li> : null}
          <li>
            Repartidor: {deliveryMan} - Entregas {deliveries}{" "}
          </li>
        </ul>
        <div className="description">
          <div className="line">
            <h1 className="product_name">Pedido #{orderNumber}</h1>
            <h1 className="product_price">{orderStatus[status]}</h1>
          </div>
          <h2>
            Cliente: {customer} - {customer_phone}
          </h2>
          <h4>Articulos: {items.length} </h4>
          <ul>
            {items.map((item, index) => {
              return (
                <ul key={index} style={{ marginLeft: -50 }}>
                  {index + 1} - {item.item}: {item.price}
                  {item.comment ? (
                    <li style={{ marginLeft: 50 }}>
                      Comentarios: {item.comment}
                    </li>
                  ) : null}
                  {item.extra ? (
                    <li style={{ marginLeft: 50 }}>Extras: {item.extra}</li>
                  ) : null}
                </ul>
              );
            })}
          </ul>
          {status === 0 || status === 3 ? (
            <button
              style={{ margin: 5 }}
              className="btn btn-success"
              onClick={async () => {
                try {
                  const response = await axios.put(
                    urlApi + `/orders/${id}`,
                    { status: 1 },
                    {
                      headers: {
                        Authorization: `${localStorage.getItem("auth_token")}`,
                      },
                    }
                  );
                  alert(response.data.message);
                  changeStatus();
                } catch (error) {
                  alert("No se pudo aceptar la orden!");
                }
              }}
            >
              Aceptar
            </button>
          ) : null}
          {status === 0 || status === 3 ? (
            <button
              style={{ margin: 5 }}
              className="btn btn-warning"
              onClick={async () => {
                try {
                  const response = await axios.put(
                    urlApi + `/orders/${id}`,
                    { status: 2 },
                    {
                      headers: {
                        Authorization: `${localStorage.getItem("auth_token")}`,
                      },
                    }
                  );
                  alert(response.data.message);
                  changeStatus();
                } catch (error) {
                  alert("No se pudo aceptar la orden!");
                }
              }}
            >
              Rechazar
            </button>
          ) : null}
          {status === 1 ? (
            <button
              style={{ margin: 5 }}
              className="btn btn-danger"
              onClick={async () => {
                try {
                  const response = await axios.put(
                    urlApi + `/orders/${id}`,
                    { status: 3 },
                    {
                      headers: {
                        Authorization: `${localStorage.getItem("auth_token")}`,
                      },
                    }
                  );
                  alert(response.data.message);
                  changeStatus();
                } catch (error) {
                  alert("No se pudo aceptar la orden!");
                }
              }}
            >
              Cancelar
            </button>
          ) : null}
        </div>
      </div>
    </>
  );
};
