const orderStatus = { 0: 'En espera', 1: 'Aceptado', 2: 'Rechazado', 3: 'Cancelado' };
export default orderStatus;