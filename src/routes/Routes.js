import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { LoginForm } from '../components/login/loginForm';
import { Orders } from "../components/orders/Orders";

export const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route 
          exact 
          path = "/"
          component = { LoginForm }
        />
        <Route 
          exact 
          path = "/orders"
          component = { Orders }
        />
      </Switch>
    </BrowserRouter>
  );
}